<refentry id="Manager">
  <refmeta>
    <refentrytitle role="top_of_page">org.freedesktop.ConsoleKit.Manager</refentrytitle>
  </refmeta>
  <refnamediv>
    <refname>org.freedesktop.ConsoleKit.Manager</refname>
    <refpurpose>Manager interface</refpurpose>
  </refnamediv>
  <refsynopsisdiv role="synopsis">
    <title role="synopsis.title">Methods</title>
    <synopsis><link linkend="Manager.Restart">Restart</link>                   ()
<link linkend="Manager.CanRestart">CanRestart</link>                (out 'b'     can_restart)
<link linkend="Manager.Stop">Stop</link>                      ()
<link linkend="Manager.CanStop">CanStop</link>                   (out 'b'     can_stop)
<link linkend="Manager.OpenSession">OpenSession</link>               (out 's'     cookie)
<link linkend="Manager.OpenSessionWithParameters">OpenSessionWithParameters</link> (in  'a(sv)' parameters,
                           out 's'     cookie)
<link linkend="Manager.CloseSession">CloseSession</link>              (in  's'     cookie,
                           out 'b'     result)
<link linkend="Manager.GetSeats">GetSeats</link>                  (out 'ao'    seats)
<link linkend="Manager.GetSessions">GetSessions</link>               (out 'ao'    sessions)
<link linkend="Manager.GetSessionForCookie">GetSessionForCookie</link>       (in  's'     cookie,
                           out 'o'     ssid)
<link linkend="Manager.GetSessionForUnixProcess">GetSessionForUnixProcess</link>  (in  'u'     pid,
                           out 'o'     ssid)
<link linkend="Manager.GetCurrentSession">GetCurrentSession</link>         (out 'o'     ssid)
<link linkend="Manager.GetSessionsForUnixUser">GetSessionsForUnixUser</link>    (in  'u'     uid,
                           out 'ao'    sessions)
<link linkend="Manager.GetSessionsForUser">GetSessionsForUser</link>        (in  'u'     uid,
                           out 'ao'    sessions)
<link linkend="Manager.GetSystemIdleHint">GetSystemIdleHint</link>         (out 'b'     idle_hint)
<link linkend="Manager.GetSystemIdleSinceHint">GetSystemIdleSinceHint</link>    (out 's'     iso8601_datetime)
</synopsis>
  </refsynopsisdiv>
  <refsect1 role="signal_proto">
    <title role="signal_proto.title">Signals</title>
    <synopsis><link linkend="Manager::SeatAdded">SeatAdded</link>             ('o' sid)
<link linkend="Manager::SeatRemoved">SeatRemoved</link>           ('o' sid)
<link linkend="Manager::SystemIdleHintChanged">SystemIdleHintChanged</link> ('b' hint)
</synopsis>
  </refsect1>
  <refsect1 role="impl_interfaces">
    <title role="impl_interfaces.title">Implemented Interfaces</title>
    <para>
    Objects implementing org.freedesktop.ConsoleKit.Manager also implements
    org.freedesktop.DBus.Introspectable,
    org.freedesktop.DBus.Properties
    </para>
  </refsect1>
  <refsect1 role="desc">
    <title role="desc.title">Description</title>
    <para/>
  </refsect1>
  <refsect1 role="details">
    <title role="details.title">Details</title>
    <refsect2><title><anchor role="function" id="Manager.Restart"/>Restart ()</title><indexterm><primary>Restart</primary><secondary>Manager</secondary></indexterm><programlisting>Restart ()</programlisting>
          <para>This method initiates a request to restart (ie. reboot) the computer system.</para>
        </refsect2>
    <refsect2>
      <title><anchor role="function" id="Manager.CanRestart"/>CanRestart ()</title>
      <indexterm>
        <primary>CanRestart</primary>
        <secondary>Manager</secondary>
      </indexterm>
      <programlisting>CanRestart (out 'b' can_restart)</programlisting>
      <variablelist role="params">
        <varlistentry>
          <term><parameter>can_restart</parameter>:</term>
          <listitem>
            <simpara/>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsect2>
    <refsect2><title><anchor role="function" id="Manager.Stop"/>Stop ()</title><indexterm><primary>Stop</primary><secondary>Manager</secondary></indexterm><programlisting>Stop ()</programlisting>
          <para>This method initiates a request to stop (ie. shutdown) the computer system.</para>
        </refsect2>
    <refsect2>
      <title><anchor role="function" id="Manager.CanStop"/>CanStop ()</title>
      <indexterm>
        <primary>CanStop</primary>
        <secondary>Manager</secondary>
      </indexterm>
      <programlisting>CanStop (out 'b' can_stop)</programlisting>
      <variablelist role="params">
        <varlistentry>
          <term><parameter>can_stop</parameter>:</term>
          <listitem>
            <simpara/>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsect2>
    <refsect2><title><anchor role="function" id="Manager.OpenSession"/>OpenSession ()</title><indexterm><primary>OpenSession</primary><secondary>Manager</secondary></indexterm><programlisting>OpenSession (out 's' cookie)</programlisting>
          <para>This method requests that a new <link linkend="Session">Session</link>
          be created for the calling process.  The properties of this new Session are set automatically
          from information collected about the calling process.
          </para>
          <para>This new session exists until the calling process disconnects from the system bus or
          calls <link linkend="Manager.CloseSession"><function>CloseSession()</function></link>.
          </para>
          <para>It is the responsibility of the calling process to set the environment variable
          XDG_SESSION_COOKIE to the value of the returned cookie.  This cookie should only
          be made available to child processes of the caller so that they may be identified
          as members of this session.
          </para>
          <para>See this simple example:
          <informalexample><programlisting>
	DBusError       error;
	DBusMessage    *message;
	DBusMessage    *reply;

	message = dbus_message_new_method_call ("org.freedesktop.ConsoleKit",
						"/org/freedesktop/ConsoleKit/Manager",
						"org.freedesktop.ConsoleKit.Manager",
						"OpenSession");
	if (message == NULL) {
		goto out;
	}

        dbus_error_init (&amp;error);
        reply = dbus_connection_send_with_reply_and_block (connector-&gt;connection,
                                                           message,
                                                           -1,
                                                           &amp;error);
        if (reply == NULL) {
		goto out;
        }

        dbus_error_init (&amp;error);
        if (! dbus_message_get_args (reply,
                                     &amp;error,
                                     DBUS_TYPE_STRING, &amp;cookie,
                                     DBUS_TYPE_INVALID)) {
		goto out;
        }

          </programlisting></informalexample></para>
        <variablelist role="params"><varlistentry><term><parameter>cookie</parameter>:</term><listitem><simpara>The secret cookie that is used to identify the new session</simpara></listitem></varlistentry></variablelist><para>
See also:
<link linkend="Manager.OpenSessionWithParameters"><function>OpenSessionWithParameters()</function></link></para></refsect2>
    <refsect2><title><anchor role="function" id="Manager.OpenSessionWithParameters"/>OpenSessionWithParameters ()</title><indexterm><primary>OpenSessionWithParameters</primary><secondary>Manager</secondary></indexterm><programlisting>OpenSessionWithParameters (in  'a(sv)' parameters,
                           out 's'     cookie)</programlisting>
          <para>This method requests that a new <link linkend="Session">Session</link>
          be created for the calling process.  The properties of this new Session are from the
          parameters provided.
          </para>
          <para>This new session exists until the calling process disconnects from the system bus or
          calls <link linkend="Manager.CloseSession"><function>CloseSession()</function></link>.
          </para>
          <para>It is the responsibility of the calling process to set the environment variable
          XDG_SESSION_COOKIE to the value of the returned cookie.  This cookie should only
          be made available to child processes of the caller so that they may be identified
          as members of this session.
          </para>
          <para>See the <link linkend="Session">Session</link> properties for a list of valid parameters.</para>
        <variablelist role="params"><varlistentry><term><parameter>parameters</parameter>:</term><listitem><simpara>An array of sets of property names and values</simpara></listitem></varlistentry><varlistentry><term><parameter>cookie</parameter>:</term><listitem><simpara>The secret cookie that is used to identify the new session</simpara></listitem></varlistentry></variablelist><note><title>Permissions</title><para role="permission">This method is restricted to privileged users by D-Bus policy.</para></note><para>
See also:
<link linkend="Session">org.freedesktop.ConsoleKit.Session</link></para></refsect2>
    <refsect2><title><anchor role="function" id="Manager.CloseSession"/>CloseSession ()</title><indexterm><primary>CloseSession</primary><secondary>Manager</secondary></indexterm><programlisting>CloseSession (in  's' cookie,
              out 'b' result)</programlisting>
          <para>This method is used to close the session identified by the supplied cookie.
          </para>
          <para>The session can only be closed by the same process that opened the session.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>cookie</parameter>:</term><listitem><simpara>The secret cookie that is used to identify the session</simpara></listitem></varlistentry><varlistentry><term><parameter>result</parameter>:</term><listitem><simpara>Whether the session was successfully closed</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSeats"/>GetSeats ()</title><indexterm><primary>GetSeats</primary><secondary>Manager</secondary></indexterm><programlisting>GetSeats (out 'ao' seats)</programlisting>
          <para>This gets a list of all the <link linkend="Seat">Seats</link>
          that are currently present on the system.</para>
          <para>Each Seat ID is an D-Bus object path for the object that implements the
          <link linkend="Seat">Seat</link> interface.</para>
        <variablelist role="params"><varlistentry><term><parameter>seats</parameter>:</term><listitem><simpara>an array of Seat IDs</simpara></listitem></varlistentry></variablelist><para>
See also:
<link linkend="Seat">org.freedesktop.ConsoleKit.Seat</link></para></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSessions"/>GetSessions ()</title><indexterm><primary>GetSessions</primary><secondary>Manager</secondary></indexterm><programlisting>GetSessions (out 'ao' sessions)</programlisting>
          <para>This gets a list of all the <link linkend="Session">Sessions</link>
          that are currently present on the system.</para>
          <para>Each Session ID is an D-Bus object path for the object that implements the
          <link linkend="Session">Session</link> interface.</para>
        <variablelist role="params"><varlistentry><term><parameter>sessions</parameter>:</term><listitem><simpara>an array of Session IDs</simpara></listitem></varlistentry></variablelist><para>
See also:
<link linkend="Session">org.freedesktop.ConsoleKit.Session</link></para></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSessionForCookie"/>GetSessionForCookie ()</title><indexterm><primary>GetSessionForCookie</primary><secondary>Manager</secondary></indexterm><programlisting>GetSessionForCookie (in  's' cookie,
                     out 'o' ssid)</programlisting>
          <para>Returns the session ID that is associated with the specified cookie.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>cookie</parameter>:</term><listitem><simpara>The secret cookie that is used to identify the session</simpara></listitem></varlistentry><varlistentry><term><parameter>ssid</parameter>:</term><listitem><simpara>The object identifier for the current session</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSessionForUnixProcess"/>GetSessionForUnixProcess ()</title><indexterm><primary>GetSessionForUnixProcess</primary><secondary>Manager</secondary></indexterm><programlisting>GetSessionForUnixProcess (in  'u' pid,
                          out 'o' ssid)</programlisting>
          <para>Attempts to determine the session ID for the specified
          POSIX process ID (pid).
          </para>
        <variablelist role="params"><varlistentry><term><parameter>pid</parameter>:</term><listitem><simpara>The POSIX process ID</simpara></listitem></varlistentry><varlistentry><term><parameter>ssid</parameter>:</term><listitem><simpara>The object identifier for the current session</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetCurrentSession"/>GetCurrentSession ()</title><indexterm><primary>GetCurrentSession</primary><secondary>Manager</secondary></indexterm><programlisting>GetCurrentSession (out 'o' ssid)</programlisting>
          <para>Attempts to determine the session ID that the caller belongs to.
          </para>
          <para>See this example of using dbus-send:
          <informalexample><programlisting>
  dbus-send --system --dest=org.freedesktop.ConsoleKit \
  --type=method_call --print-reply --reply-timeout=2000 \
  /org/freedesktop/ConsoleKit/Manager \
  org.freedesktop.ConsoleKit.Manager.GetCurrentSession
          </programlisting></informalexample></para>
        <variablelist role="params"><varlistentry><term><parameter>ssid</parameter>:</term><listitem><simpara>The object identifier for the current session</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSessionsForUnixUser"/>GetSessionsForUnixUser ()</title><indexterm><primary>GetSessionsForUnixUser</primary><secondary>Manager</secondary></indexterm><programlisting>GetSessionsForUnixUser (in  'u'  uid,
                        out 'ao' sessions)</programlisting>
          <para>This gets a list of all the <link linkend="Session">Sessions</link>
          that are currently open for the specified user.</para>
          <para>Each Session ID is an D-Bus object path for the object that implements the
          <link linkend="Session">Session</link> interface.</para>
        <variablelist role="params"><varlistentry><term><parameter>uid</parameter>:</term><listitem><simpara>POSIX User identification</simpara></listitem></varlistentry><varlistentry><term><parameter>sessions</parameter>:</term><listitem><simpara>an array of Session IDs</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSessionsForUser"/>GetSessionsForUser ()</title><indexterm><primary>GetSessionsForUser</primary><secondary>Manager</secondary></indexterm><programlisting>GetSessionsForUser (in  'u'  uid,
                    out 'ao' sessions)</programlisting>
          <para>This gets a list of all the <link linkend="Session">Sessions</link>
          that are currently open for the specified user.</para>
          <para>Each Session ID is an D-Bus object path for the object that implements the
          <link linkend="Session">Session</link> interface.</para>
        <variablelist role="params"><varlistentry><term><parameter>uid</parameter>:</term><listitem><simpara>User identification</simpara></listitem></varlistentry><varlistentry><term><parameter>sessions</parameter>:</term><listitem><simpara>an array of Session IDs</simpara></listitem></varlistentry></variablelist><para role="deprecated"><warning><para><literal>GetSessionsForUser</literal> is deprecated since version 0.1.3 and should not be used in newly-written code. Use

  <link linkend="Manager.GetSessionsForUnixUser"><function>GetSessionsForUnixUser</function></link>
instead.</para></warning></para></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSystemIdleHint"/>GetSystemIdleHint ()</title><indexterm><primary>GetSystemIdleHint</primary><secondary>Manager</secondary></indexterm><programlisting>GetSystemIdleHint (out 'b' idle_hint)</programlisting>
          <para>Returns TRUE if the <link linkend="Session:idle-hint"><literal>idle-hint</literal></link>
          property of every open session is TRUE or if there are no open sessions.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>idle_hint</parameter>:</term><listitem><simpara>The value of the system-idle-hint</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager.GetSystemIdleSinceHint"/>GetSystemIdleSinceHint ()</title><indexterm><primary>GetSystemIdleSinceHint</primary><secondary>Manager</secondary></indexterm><programlisting>GetSystemIdleSinceHint (out 's' iso8601_datetime)</programlisting>
          <para>Returns an ISO 8601 date-time string that corresponds to
          the time of the last change of the system-idle-hint.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>iso8601_datetime</parameter>:</term><listitem><simpara>An ISO 8601 format date-type string</simpara></listitem></varlistentry></variablelist></refsect2>
  </refsect1>
  <refsect1 role="signals">
    <title role="signals.title">Signal Details</title>
    <refsect2><title><anchor role="function" id="Manager::SeatAdded"/>The SeatAdded signal</title><indexterm><primary>SeatAdded</primary><secondary>Manager</secondary></indexterm><programlisting>SeatAdded ('o' sid)</programlisting>
          <para>Emitted when a Seat has been added to the system.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>sid</parameter>:</term><listitem><simpara>The Seat ID for the added seat</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager::SeatRemoved"/>The SeatRemoved signal</title><indexterm><primary>SeatRemoved</primary><secondary>Manager</secondary></indexterm><programlisting>SeatRemoved ('o' sid)</programlisting>
          <para>Emitted when a Seat has been removed from the system.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>sid</parameter>:</term><listitem><simpara>The Seat ID for the removed seat</simpara></listitem></varlistentry></variablelist></refsect2>
    <refsect2><title><anchor role="function" id="Manager::SystemIdleHintChanged"/>The SystemIdleHintChanged signal</title><indexterm><primary>SystemIdleHintChanged</primary><secondary>Manager</secondary></indexterm><programlisting>SystemIdleHintChanged ('b' hint)</programlisting>
          <para>Emitted when the value of the system-idle-hint has changed.
          </para>
        <variablelist role="params"><varlistentry><term><parameter>hint</parameter>:</term><listitem><simpara>The value of the system-idle-hint</simpara></listitem></varlistentry></variablelist></refsect2>
  </refsect1>
</refentry>
